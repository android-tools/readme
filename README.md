# Get APKs

- androzoo: https://androzoo.uni.lu/
    + academics can have an API Key
- https://raccoon.onyxbits.de/
- https://www.sec.cs.tu-bs.de/~danarp/drebin/download.html

# Automate UI testing

- [python] androidviewclient: https://github.com/dtmilano/AndroidViewClient

# Emulators

- [docker] https://github.com/google/android-emulator-container-scripts
    + on construit quelques images ici: https://gitlab.inria.fr/android-tools/emulator/container_registry

# Static analysis tools

## assembling/disassembling

- https://github.com/JesusFreke/smali
- https://github.com/skylot/jadx

## General tools

- http://soot-oss.github.io/soot/
- https://blogs.uni-paderborn.de/sse/tools/flowdroid/
- https://androguard.readthedocs.io

